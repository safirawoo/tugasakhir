#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

long int flag, e[100], d[100], temp[100], j, i, pr, k;
char m[100], en[100], pesan[100];
int p, q, n, t;
int prime(long int);
long int cd(long int);
void ce();
void random_prima();

int main()
{
    printf("--------Program Pembangkitan Kunci RSA-------\n\n");

    random_prima(); //memanggil f
    ce();           //cek nilai enkripsi

    int pri = d[0];
    int pub = e[0];
    FILE *kunci;
    kunci = fopen("kunci.txt", "w");
    fprintf(kunci, "p=%d\nq=%d\nn=%d\ne=%d\nd=%d\n", p, q, n, pub, pri);
    fclose(kunci);

    printf("\nANGKA TERSEDIA DARI E DAN D");
    for (i = 0; i < j - 1; i++)
    {
        printf("\nNILAI E: %ld\t  NILAI D: %ld", e[i], d[i]);
    }
    printf("\n");
    getch();
}

int prime(long int pr)
{
    int i;
    j = sqrt(pr);
    for (i = 2; i <= j; i++)
    {
        if (pr % i == 0)
            return 0;
    }
    return 1;
}

void ce()
{
    int k;
    k = 0;
    for (i = 2; i < t; i++)
    {
        if (t % i == 0)
            continue;
        flag = prime(i);
        if (flag == 1 && i != p && i != q)
        {
            e[k] = i; //nilai enkripsi
            flag = cd(e[k]);
            if (flag > 0)
            {
                d[k] = flag; //nilai dekripsi
                k++;
            }
            if (k == 99)
                break;
        }
    }
}

long int cd(long int x)
{
    long int k = 1;
    while (1)
    {
        k = k + t;
        if (k % x == 0)
            return (k / x);
    }
}

void random_prima()
{
    int p, q;
    int i, number[1000] = {0}, temp, j, status = 0, prime[1000] = {0}, r;
    for (i = 2; i <= 1000; i++)
    {
        for (j = i + 1; j <= 1000; j++)
        {
            if (j % i == 0 && number[j] == 0)
            {
                number[j] = 1;
            }
        }
    }
    temp = 0;
    for (i = 2; i <= 1000; i++)
    {
        if (number[i] == 0)
        {
            prime[temp++] = i;
        }
    }
    srand(time(NULL));
    do
    {
        r = rand() % 10;
    } while (prime[r] == 0);
    p = prime[r];
    do
    {
        r = rand() % 10;
    } while (prime[r] == 0 || prime[r] == p);
    {
        q = prime[r];
    }
    printf("Bilangan Prima 1 : %d", p);
    printf("\nBilangan Prima 2 : %d", q);

    n = p * q; // perkalian pembangkitan kunci
    printf("\nn = %d \n", n);
    t = (p - 1) * (q - 1); //nilai untuk mencari relatif prima kunci
}
