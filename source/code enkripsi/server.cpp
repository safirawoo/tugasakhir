#include <stdio.h>
#include <string.h>
#include <ncurses.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>	
#include <time.h>
#include <math.h>
#include "state.txt"

//-----------inisialisasi variabel server---------------
typedef unsigned int uint;
char arrPort[10000][5];
int total = 0;
int counter = 0, counter2 = 0, size_text;
int flag_oc = 0;
void myrandom();
void get_rand_from_file();
void remove_string(char *);

//-----------inisialisasi variabel RSA---------------
long int flag,temp[100],j,i,pr,k;
char m[100], en[100], pesan[100], pilih;
long int  prima1, prima2, a,status,cek;

struct cipher_type {
	int k;
	int en_char;
	char encrypt_char;
};

//--- functions
int p, q, n, t, e[100],d[100];
int prime(long int);
long int cd(long int);
void ce();
void random_prima();
void encrypt();
void decrypt();
int is_encrypted(char *);

int main(int argc, char *argv[])
{
  int socket_desc, client_sock, client_sock2, c, c2, read_size, read_size2, sock2;
  struct sockaddr_in server, client, server2, client2;
  char client_message[2000] = {0};
  char client_message2[2000] = {0};
  int cont = 1;
  int port_baru, port_default;
  int last_socket = 0;
  char temp[5], close_msg[5];

  char response_ok[2000] = "HTTP/1.1 200 Ok\nContent-type:text/html\n\n<html>\n<body>\n<h1>Server Hello</h1>\n</body>\n</html>\n";
  //char response_ok[2000] df= "HTTP/1.1 200 Ok\nContent-type:text/html\n\n<html>\n<body>\n<h1>Server Hello</h1>\n</body>\n</html>\n";
  size_t i;

  close_msg[0] = 'e';
  close_msg[1] = 'n';
  close_msg[2] = 'd';
	
  printf("\033[0;32m\nMenutup Port 1016...\033[0m\n");
  sleep(2);
  system("iptables -A INPUT -p tcp --dport 1016 -j REJECT");
  system("iptables -I INPUT -p tcp --dport 1016 -j REJECT");
  printf("Port 1016 Ditutup\n\n");
	
  //initialization
  int cnt = 0;
  port_default = 80;
  port_baru = port_default;
  koneklagi:

  //membuat socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if(setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
  {
    perror("setsockopt(SO_REUSEADDR) failed");
  }
  if(socket_desc == -1)
  {
    printf("tidak bisa membuat socket");
  }
  puts("Socket telah dibuat");
	 
  //prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons( port_baru );
		
  //bind
  rebind:
  if( bind(socket_desc, (struct sockaddr *)&server , sizeof(server)) < 0)
  {
  
    //print the error message
    perror("bind failed. Error");
    printf("\nServer will rebind in 3 seconds...\n");
    close(socket_desc);
    sleep(1);
    goto koneklagi;
  }
  puts("Bind sukses");
	
  //listen
  listen(socket_desc, 3);	
  tunggu:

  //accept and incoming connection
  puts("Menunggu koneksi masuk...");
  c = sizeof(struct sockaddr_in);
	
  //accept connection perform an incoming client
  client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
  if(client_sock < 0)
  {
    goto tunggu;
  }
  puts("Connection accepted");
	
  //receive a message from client
  while(cont == 1)
  {
    printf("\nMenunggu.....\n");
    read_size = read(client_sock, client_message, 2000);
    printf("msg %d: %s \n", cnt, client_message);
    remove_string(client_message);
    
    //-- check whether client_message is encrypted or not
    if(strcmp(client_message, "tes\0") != 0){
    //if ( is_encrypted(client_message) != 0 ) {
		//--- it did encrypted
		//-- simpan client_message ke file
		char filename[] = "clMsgTemp.tmp", filePlain[] = "clmsg.txt", buffer[100];
		FILE *cl_message_file, *cl_message;
		struct cipher_type byte_cipher;	
		
		cl_message_file = fopen(filename, "wb");
		fwrite(&client_message, sizeof(client_message), 1, cl_message_file);
		fclose(cl_message_file);
		
		//-- decrypt client_message;
		char shell_command[200];
		strcpy( shell_command, "./rsa -f decrypt clMsgTemp.tmp " );
		// strcpy( shell_command, "./rsa -d decrypt " );
		// strcat( shell_command, client_message);
		strcat( shell_command, " ");		
		strcat( shell_command, filePlain );
		printf("command : %s", shell_command);
		system( shell_command );
		
		//-- read from clMsg.txt
		cl_message = fopen(filePlain, "r");
		fread(buffer, sizeof(buffer), 1, cl_message);
		fclose(cl_message);
		printf("Buffer : %s", buffer);
		strcpy( client_message, buffer );
	} //-- else, just going on
	
    //cek karakter
    printf("%ld",strlen(client_message));
		
    //cek ASCII
    for(i=0;i<strlen(client_message);i++)
    {
      printf("%d  ",client_message[i]);
    }
    if(strcmp(client_message, "close") == 0)
    {
      if(flag_oc == 1)
      {
        printf("\n Closing Port 1016....\n");
	sleep(2);
	system("iptables -A INPUT -p tcp --dport 1016 -j REJECT");
	system("iptables -I INPUT -p tcp --dport 1016 -j REJECT");
	printf("Port 1016 Closed\n\n");
      }
      else
      {
	printf("\nYou have to knocking port first\n");
      }
    }
    if(strcmp(client_message, "open") == 0)
    {
      if(flag_oc == 1)
      {
	printf("\n Opening port 23...\n");
	sleep(2);
	system("iptables -A INPUT -p tcp --dport 1016 -j ACCEPT");
	system("iptables -I INPUT -p tcp --dport 1016 -j ACCEPT");
	printf("Port 1016 opened\n\n");
      }
      else
      {
	printf("\nYou have to knocking port first\n");
      }
    }
    if(strcmp(client_message, "quit_server") == 0)
    {
      return 1;
      break;
    }

    if(strcmp(client_message, "tes") == 0)
    {
      printf("\nMelakukan Proses Random Port...\n");
      myrandom();
      get_rand_from_file();
      sleep(1);
      printf("\nRandom Port Siap...\n");
      cnt = 0;
      sleep(2);
      system("iptables -A INPUT -p tcp --dport 1016 -j REJECT");
      system("iptables -I INPUT -p tcp --dport 1016 -j REJECT");
      printf("Port 1016 Closed\n\n");
      write(client_sock, arrPort[cnt], 5);

      //Penyimpanan port sementara
      char temp[5];
      temp[0] = arrPort[cnt][0];
      temp[1] = arrPort[cnt][1];
      temp[2] = arrPort[cnt][2];
      temp[3] = arrPort[cnt][3];
      temp[4] = arrPort[cnt][4];
      port_baru = atoi(temp);
      cnt++;
      close(socket_desc);
      printf("\nServer Listen pada Port : %d\n", port_baru);
      printf("\n\n");
      goto koneklagi;
    }

    if(client_message[0] == 'G' && client_message[1] == 'E' && client_message[2] == 'T')
    {
      write(client_sock, response_ok, 2000);
      close(client_sock);
      close(socket_desc);
      printf("%d\n", port_baru);
      goto koneklagi;
    }
    //-- client_message ditulis ke client_msg.txt
    // panggil rsa_arg dengan argumen decrypt client_msg.txt ditulis di file client_plain.txt
    // baca client_plain.txt
    // compare hasil decrypt(client_plain) dengan perintah knock    
    if(strcmp(client_message, "knock") == 0)
    {
      if(cnt == 10)
      //if(cnt == total)
      {
        printf("\nPort telah habis...\n");
        flag_oc = 1;
        write(client_sock, close_msg, 5);
        printf("\nMembuka Port 1016...\n");
        sleep(2);
        system("iptables -A INPUT -p tcp --dport 1016 -j ACCEPT");
        system("iptables -I INPUT -p tcp --dport 1016 -j ACCEPT");
        printf("Port 1016 Opened\n\n");
        port_baru = port_default;
        close(client_sock);
        close(socket_desc);
        goto koneklagi;
      }
      printf("\nKnock sukses pada port : %d\n",port_baru);
      write(client_sock, arrPort[cnt], 5);
      char temp[5];
      temp[0] = arrPort[cnt][0];
      temp[1] = arrPort[cnt][1];
      temp[2] = arrPort[cnt][2];
      temp[3] = arrPort[cnt][3];
      temp[4] = arrPort[cnt][4];
      port_baru=atoi(temp);
      cnt++;
      close(socket_desc);
      printf("\nServer Listen pada Port : %d\n",port_baru);
      printf("\n");

      goto koneklagi;
    }
    if(strcmp(client_message, "quit") == 0)
    {
      close(client_sock);
      close(socket_desc);
      goto koneklagi;
    }
    if(strcmp(client_message, "") == 0)
    {
      close(client_sock);
      close(socket_desc);
      goto koneklagi;
    }
    else
    {
      char t_msg[5];
      write(client_sock, t_msg, 5);
    }
  }
 goto koneklagi;
}

int is_encrypted(char *str)
{
	int ret = 0;
	
	if (str[0] == 'e' && str[1] == 'n' && str[2] == 'c') {
		//-- enc
		ret = 0;
	} else {
		//-- no enc
		ret = -1;
	}
	
	return ret;
}
		
void get_rand_from_file()
{
  int counter;
  char ports[65000];
  char* xxx;
  char* filename ="./acak.txt";
	
  FILE* file = fopen(filename, "r");
  char line[65000];
  while(fgets(line, sizeof(line), file))
  {
    strcpy(ports, line);
  }
  fclose(file);
  size_text = strlen(ports);
  counter = 0;
  total = 0; 
  while(1)
  {
    if(ports[counter] == ',')
    {
      total++;
    }
    if(counter == size_text)
    {
      break;
    }
    counter++;
  }

  //sebagai pemecah koma
  counter=0; 
  
  //sebagai pemecah port
  counter2=0;

  //untuk 5 angka port
  int counter3 = 0;
  while(counter < size_text)
  {
    if(ports[counter] == ',')
    {
      counter2++;
      counter3=0;
      arrPort[counter2][0] = ' ';
      arrPort[counter2][1] = ' ';
      arrPort[counter2][2] = ' ';
      arrPort[counter2][3] = ' ';
      arrPort[counter2][4] = ' ';
    }
    else
    {
      arrPort[counter2][counter3] = ports[counter];
      counter3++;
    }
    counter++;
  }
}
	
typedef struct 
{ 
  uint64_t state;  
  uint64_t increment; 
  uint64_t multiplier;
}
 
pcg32_random_t;
//static uint64_t state = 0xcafef00dd15ea5e5;
static uint64_t const multiplier = 6364136223846794005u;
static uint64_t const increment = 1442695040888963407u;	

uint32_t pcg32_random_r(pcg32_random_t* rng)
{
  uint64_t oldstate = rng->state;
  rng->state = oldstate * multiplier + (rng->increment|1);
  uint32_t xorshifted = ((oldstate >> 18) ^ oldstate) >> 27;
  uint32_t rot = oldstate >> 59;
  return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

void pcg32_srandom_r(pcg32_random_t* rng, uint64_t initstate, uint64_t initseq)
{
  rng->state = 0u;
  rng->increment = (initseq << 1u) | 1u;
  pcg32_random_r(rng);
  rng->state += initstate;
  pcg32_random_r(rng);
}

void myrandom()
{
  FILE *filea;
  FILE *fileb;
  int random_port, random, flag=0,j;
  int max_rand_bil = 0;
  int counter = 0;
  int cntr = 0;
  int temp_random[10000];
  char buffer[99999];
  char portrandom[99999];
  size_t i;
  //static uint64_t state = 0x0000b711;
	
  filea = fopen("acak.txt", "w");
  fileb = fopen("state.txt", "w");
  pcg32_random_t pcg;
  //pcg32_srandom_r(&pcg, multiplier, increment);
  pcg32_srandom_r(&pcg, state, multiplier);
	
  max_rand_bil = (pcg32_random_r(&pcg) % 100 + 1);
  //max_rand_bil = (pcg32_random_r(&pcg) % 10000) + 1259;
  //max_rand_bil = pcg32_random_r(&pcg) % 65000;
  while(1)
  {
    random_port = pcg32_random_r(&pcg) % 65000; //hasil port < 65.000
    flag = 0;
    for(i = 0; i < max_rand_bil; i++)
    {
      if(random_port == temp_random[i])
      {
	flag = 1;
      }
      if (random_port == 8080 || random_port == 23 || random_port == 80 || random_port == 23)
      {
        flag = 1;
      }
    }
    if(flag == 1)
    {
      continue;
    }
    else
    {
      sprintf(buffer,"%d", random_port); //untuk decimal
      fwrite(buffer, strlen(buffer), 1, filea);
      temp_random[j] = random_port;
      cntr++;  
     
    }
    if(cntr == max_rand_bil)
    {
      break;
    }
  }
  //printf("random yg dimasukkan : %0x%x", temp_random[i]);
  fprintf(fileb, "static uint64_t state = 0x%08x;", temp_random[0]);
  fclose(filea);
  fclose(fileb);
}

//fungsi hapus enter
void remove_string(char *str)
{
  if(str == NULL)
  return;
  int length = strlen(str);
  if(str[length-1] == '\n')
  str[length-1] = '\0';
}
