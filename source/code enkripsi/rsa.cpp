#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

struct cipher_type
{
  int k;
  int en_char;
  char encrypt_char;
};

long int flag, temp[100], j, i, pr;
char m[100], en[100];

int p = 29;
int q = 7;
int n = 203;
int e = 5;
int d = 101;

//-- directly functions
void encrypt(char *, char *);
void decrypt(char *, char *);

//-- from file functions
void encrypt_f(char *, char *);
void decrypt_f(char *, char *);

int main(int argc, char **argv)
{
  printf("\033[0;32mRSA Encryption and Decryption\033[0m \n\n");
  if (argc == 1)
  {
    printf("How to use:\n\033[0;33m [directly]\033[0m");
    printf(" %s ", argv[0]);
    printf("-d <encrypt|decrypt> <message/pesan> <file to write>\n\n");
    printf(" \033[0;33m[from file]\033[0m");
    printf(" %s ", argv[0]);
    printf("-f <encrypt|decrypt> <file to open> <file to write>\n\n");
    printf("\033[1;31mYou provide an invalid arguments.\n\033[0m");
  }
  else if (argc > 1)
  {
    printf("Argument counter: %d\n", argc);
    if (argc == 5)
    {
      char *mode = argv[1];
      char *command = argv[2];
      char *message = argv[3];
      char *filename = argv[4];
      printf("Mode: %s\n", mode);

      if (strcmp(mode, "-d\0") == 0)
      {
        printf("Command: %s \nMessage: %s\nFile to be written: %s\n", command, message, filename);

	if (strcmp(command, "encrypt\0") == 0)
	{
	//--- encrypt
	  printf("Encrypting message: %s\n", message);
	  encrypt(message, filename);
	//printf("Decrypt try:\n");
	//decrypt(en, filename);
	//printf("Decrypt_f try:\n");
	//decrypt_f(filename, "plain1.txt");
	}
	else if (strcmp(command, "decrypt\0") == 0)
	{
	//--- decrypt
	  printf("Decrypting message: %s\n", message);
	  decrypt(message, filename);
	}
      }
      else if (strcmp(mode, "-f\0") == 0)
      {
        printf("Command: %s \nFile to be opened: %s\nFile to be written: %s\n", command, message, filename);

	if (strcmp(command, "encrypt\0") == 0)
	{
	//--- encrypt
	  printf("Encrypting file: %s\n", message);
	  encrypt_f(message, filename);
	}
	else if (strcmp(command, "decrypt\0") == 0)
	{
	//--- decrypt
	  printf("Decrypting file: %s\n", message);
	  decrypt_f(message, filename);
	}
      }
      else
      {
        printf("Mode arguments required.\n");
      }
    }
    else
    {
      printf("Minimal arguments required.\n");
    }
  }
  printf("\033[0;36mExitting program.\n\033[0m");
}

//------------------enkripsi pesan----------------------
void encrypt(char *pesan, char *filename)
{
  int pt, ct, key = e, k, len;
  i = 0;
  struct cipher_type byte_cipher;
  FILE *filea;

  filea = fopen(filename, "wb");

  // menghitung panjang karakter dalam suatu string
  // program akan selesai jika nilai len sudah berakhir
  len = strlen(pesan);
  printf("Pesan: %s \tPanjang: %d\n", pesan, len);
  while (i != len)
  {
    //menampung nilai message ke variabel pt
    pt = pesan[i];
    pt = pt - 96;
    k = 1;

    for (j = 0; j < key; j++)
    {
      k = k * pt;
      k = k % n;
    }
    temp[i] = k; //menampung nilai k
    ct = k + 96;
    en[i] = ct;

    //--- for tracing
    //printf("--- tracing encrypt\n");
    //printf("= en: %c - k: %d\n", en[i], k);

    //-- write to file as text
    byte_cipher.k = k;
    byte_cipher.encrypt_char = en[i];
    byte_cipher.en_char = (int)en[i];
    printf("byte_cipher = en: %c %d - k: %d\n", byte_cipher.encrypt_char, en[i], byte_cipher.k);

    fwrite(&byte_cipher, sizeof(struct cipher_type), 1, filea);
    i++;
  }
  en[i] = -1;
  //printf("En: %s \nPanjang en: %lu \n", en, sizeof(en));
  fclose(filea);
}

//------------------dekripsi pesan----------------------
void decrypt(char *cipher, char *filename)
{
  long int pt, ct, key = d, k;
  i = 0;
  FILE *fileb;
  fileb = fopen(filename, "r");

  printf("Cipher: %s \n", cipher);
  //while (en[i]!=-1) {
  while (cipher[i] != -1)
  {
    ct = temp[i]; // k
    //printf("  - temp: %li \n", ct);
    //printf("  - cipher: %c \n", cipher[i]);

    k = 1; //ct-96;
    for (j = 0; j < key; j++)
    {
      k = k * ct;
      k = k % n;
    }
    pt = k + 96;
    m[i] = pt;
    i++;
  }
  m[i] = -1;
  // for tracing only
  printf("--- tracing decrypt\n");
  for (i = 0; m[i] != -1; i++)
  {
    printf("%c", m[i]);
  }
  printf("\nM: %s \n", m);
  //fprintf(fileb,"%s",m);
  fclose(fileb);
}

//------------------enkripsi pesan from file---------------------
void encrypt_f(char *file_pesan, char *filename)
{
  int pt, ct, key = e, k, len;
  i = 0;
  FILE *filea, *fileb;

  if ((filea = fopen(file_pesan, "rb")) == NULL)
  {
    printf("Error opening file: %s", file_pesan);
    exit(1);
  }
  fread(&m, sizeof(m), 1, filea);
  fclose(filea);

  // menghitung panjang karakter dalam suatu string
  // program akan selesai jika nilai len sudah berakhir
  char *pesan = m;
  len = strlen(pesan); 
  while (i != len)
  {
    //menampung nilai message ke variabel pt
    pt = m[i];
    // pt=pt-96;
    k = 1;
    for (j = 0; j < key; j++)
    {
      k = k * pt;
      k = k % n;
    }
    temp[i] = k; //menampung nilai k
    ct = k;
    // ct = k + 96;
    en[i] = k;
    // printf("%ld %x\n", i, ct);
    i++;
  }
  en[i] = 0;
  fileb = fopen(filename, "wb");
  fprintf(fileb, "%s", en);
  fclose(fileb);
}

//------------------dekripsi pesan from file----------------------
void decrypt_f(char *file_cipher, char *filename)
{
  long int key = d, k;
  unsigned char pt, ct;
  char buffer[255];
  i = 0;
  struct cipher_type byte_cipher;
  FILE *filea, *fileb;

  if ((filea = fopen(file_cipher, "rb")) == NULL)
  {
    printf("Error opening file: %s", file_cipher);
    exit(1);
  }

  // fucntion used to read the contents of file
  fread(en, sizeof(en), 1, filea);

  // while (fread(&byte_cipher, sizeof(struct cipher_type), 1, filea))
  // {
  // 	//-- read from file as block
  // 	temp[i] = byte_cipher.k;
  // 	ct = byte_cipher.encrypt_char;
  // 	en[i] = ct;
  // 	//printf ("-- temp-%li: %li - ct: %li \n", i, temp[i], ct);
  // 	i++;
  // }
  //printf("\ncontent: %s\n", en);
  // en[i] = '\0';

  i = 0;
  while (en[i] != '\0')
  {
    ct = en[i];
    // printf("%ld %d %d\n", i, ct, en[i]);
    k = 1;
    for (j = 0; j < key; j++)
    {
      k = k * ct;
      k = k % n;
    }
    pt = k;
    m[i] = pt;
    // printf("%ld %x\n", i, k);
    i++;
  }
  m[i] = '\0';
  fclose(filea);

  // for tracing only
  //for (i=0; m[i]!=-1; i++) {
  //	printf("%c",m[i]);
  //}

  fileb = fopen(filename, "w");
  fprintf(fileb, "%s", m);
  fclose(fileb);
}

//-- eof
