/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_About;
    QAction *action_Open;
    QAction *action_Save;
    QAction *action_Encrypt;
    QAction *action_Decrypt;
    QAction *action_Exit;
    QAction *action_SaveAll;
    QAction *action_SavePlaintext;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QPushButton *btBrowsePlain;
    QTextEdit *txEditPlain;
    QLineEdit *txFilenamePlain;
    QLabel *label;
    QGroupBox *groupBox_2;
    QLineEdit *txFilenameCipher;
    QLabel *label_2;
    QPushButton *btBrowseCipher;
    QTextEdit *txEditCipher;
    QMenuBar *menuBar;
    QMenu *menu_File;
    QMenu *menu_Help;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(600, 511);
        action_About = new QAction(MainWindow);
        action_About->setObjectName(QStringLiteral("action_About"));
        action_Open = new QAction(MainWindow);
        action_Open->setObjectName(QStringLiteral("action_Open"));
        action_Save = new QAction(MainWindow);
        action_Save->setObjectName(QStringLiteral("action_Save"));
        action_Encrypt = new QAction(MainWindow);
        action_Encrypt->setObjectName(QStringLiteral("action_Encrypt"));
        action_Decrypt = new QAction(MainWindow);
        action_Decrypt->setObjectName(QStringLiteral("action_Decrypt"));
        action_Exit = new QAction(MainWindow);
        action_Exit->setObjectName(QStringLiteral("action_Exit"));
        action_SaveAll = new QAction(MainWindow);
        action_SaveAll->setObjectName(QStringLiteral("action_SaveAll"));
        action_SavePlaintext = new QAction(MainWindow);
        action_SavePlaintext->setObjectName(QStringLiteral("action_SavePlaintext"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 581, 241));
        btBrowsePlain = new QPushButton(groupBox);
        btBrowsePlain->setObjectName(QStringLiteral("btBrowsePlain"));
        btBrowsePlain->setGeometry(QRect(480, 30, 89, 25));
        txEditPlain = new QTextEdit(groupBox);
        txEditPlain->setObjectName(QStringLiteral("txEditPlain"));
        txEditPlain->setGeometry(QRect(10, 60, 561, 171));
        txFilenamePlain = new QLineEdit(groupBox);
        txFilenamePlain->setObjectName(QStringLiteral("txFilenamePlain"));
        txFilenamePlain->setGeometry(QRect(90, 30, 381, 25));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(11, 34, 81, 17));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 260, 580, 195));
        txFilenameCipher = new QLineEdit(groupBox_2);
        txFilenameCipher->setObjectName(QStringLiteral("txFilenameCipher"));
        txFilenameCipher->setGeometry(QRect(90, 30, 381, 25));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(11, 34, 81, 17));
        btBrowseCipher = new QPushButton(groupBox_2);
        btBrowseCipher->setObjectName(QStringLiteral("btBrowseCipher"));
        btBrowseCipher->setGeometry(QRect(480, 30, 89, 25));
        txEditCipher = new QTextEdit(groupBox_2);
        txEditCipher->setObjectName(QStringLiteral("txEditCipher"));
        txEditCipher->setGeometry(QRect(10, 60, 561, 121));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 22));
        menu_File = new QMenu(menuBar);
        menu_File->setObjectName(QStringLiteral("menu_File"));
        menu_Help = new QMenu(menuBar);
        menu_Help->setObjectName(QStringLiteral("menu_Help"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu_File->menuAction());
        menuBar->addAction(menu_Help->menuAction());
        menu_File->addAction(action_Open);
        menu_File->addAction(action_Save);
        menu_File->addAction(action_SavePlaintext);
        menu_File->addAction(action_SaveAll);
        menu_File->addSeparator();
        menu_File->addAction(action_Encrypt);
        menu_File->addAction(action_Decrypt);
        menu_File->addSeparator();
        menu_File->addAction(action_Exit);
        menu_Help->addAction(action_About);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        action_About->setText(QApplication::translate("MainWindow", "&About", Q_NULLPTR));
        action_Open->setText(QApplication::translate("MainWindow", "&Open", Q_NULLPTR));
        action_Save->setText(QApplication::translate("MainWindow", "&Save Ciphertext", Q_NULLPTR));
        action_Encrypt->setText(QApplication::translate("MainWindow", "&Encrypt", Q_NULLPTR));
        action_Decrypt->setText(QApplication::translate("MainWindow", "&Decrypt", Q_NULLPTR));
        action_Exit->setText(QApplication::translate("MainWindow", "E&xit", Q_NULLPTR));
        action_SaveAll->setText(QApplication::translate("MainWindow", "Save &All", Q_NULLPTR));
        action_SavePlaintext->setText(QApplication::translate("MainWindow", "Save &Plaintext", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Plaintext", Q_NULLPTR));
        btBrowsePlain->setText(QApplication::translate("MainWindow", "&Browse", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "File name :", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Ciphertext", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "File name :", Q_NULLPTR));
        btBrowseCipher->setText(QApplication::translate("MainWindow", "B&rowse", Q_NULLPTR));
        menu_File->setTitle(QApplication::translate("MainWindow", "&File", Q_NULLPTR));
        menu_Help->setTitle(QApplication::translate("MainWindow", "&Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
