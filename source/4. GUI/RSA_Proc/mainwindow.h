#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

/* LIBRARIES  */
#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void closeApp();
    void openPlainText();
    void openCipherText();
    void saveAll();

private:
    Ui::MainWindow *ui;

    /* Variables for MainWindow */
    long int flag,e[100],d[100],temp[100],j,i,pr,k;
    char m[100], en[100], pesan[100], pilih;
    long int  prima1, prima2, a,status,cek;
    int p, q, n, t;

};

#endif // MAINWINDOW_H
