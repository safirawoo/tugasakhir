#include "mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QRect screenGeometry = QApplication::desktop()->screenGeometry();

    int x = 0, y = 0;
    x = (screenGeometry.width() - w.width()) / 2;
    y = (screenGeometry.height() - w.height()) / 2;

    w.move(x, y);
    w.show();

    return a.exec();
}
