#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(600, 511);

    ui->statusBar->showMessage("Ready.");

    // Menu Bar actions
    connect( ui->action_Exit, SIGNAL (triggered()), this, SLOT (closeApp()) );

    // Radio Button actions
    connect( ui->action_Open, SIGNAL (triggered()), this, SLOT (openPlainText()) );
    connect( ui->action_Encrypt, SIGNAL (triggered()), this, SLOT (openPlainText()) );
    connect( ui->action_Decrypt, SIGNAL (triggered()), this, SLOT (openPlainText()) );
}

void MainWindow::closeApp()
{
    ui->statusBar->showMessage("Exiting application...");
    qApp->exit();
}

void MainWindow::openPlainText()
{

}

void MainWindow::openCipherText()
{

}

void MainWindow::saveAll()
{

}

MainWindow::~MainWindow()
{
    delete ui;
}
