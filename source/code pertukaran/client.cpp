#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <math.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//-----------inisialisasi variabel RSA---------------
long int flag,e[100],d[100],temp[100],j,i,pr,k;
char m[100], en[100], pesan[100], pilih;
long int  prima1, prima2, a,status,cek;
int p, q, n, t;

int prime(long int);
long int cd(long int);
void ce();
void decrypt();

int main(int argc, char *argv[])
{
  int sock, sock2;
  struct sockaddr_in server, server2;
  char message[1000], server_reply[2000], server_reply2[2000];
  int counter = 0, counter2 = 0, total = 0, size_text;
  char arrPort[10000][5];
  char message2[1000] = "knock";
  char message3[1000];
  int port_baru, port_default;
	
  //inisialisasi
  port_default = 80;
  port_baru = port_default;
  koneklagi:

  //create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock == -1)
  {
    printf("Could not create socket");
  }
  puts("Socket created");
  //puts (argv[0]);
	
  if(server.sin_addr.s_addr = inet_addr(argv[1]))
  {
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons( port_baru );
    printf("alamat IP benar\n");
  }
  else
  {
    printf("IP salah\n");
    return 0;
  }
	
  if(connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
  {
    perror("Connect failed. ERROR");
    printf("\ntrying reconnecting to server (port %d)...\n", port_baru);
    close(sock);
    sleep(2);
    goto koneklagi;
  }
  puts("Connected\n");
	
  sendrequestagain:
  if(port_baru == port_default)
  {
    printf("Masukkan Pesan: ");
    scanf("%s", message);
    if(strcmp(message,"quit") == 0)
    {
      write(sock, message, sizeof(message));
      printf("\nMenutup Program...\n");
      sleep(2);
      return 1;
    }
    if(strcmp(message,"key") == 0)
    {
      char kunci[200];
      FILE *publik;
      publik = fopen("public_client.txt", "r");
      fread(kunci, 200, 1, publik);
      printf("\n\n\nkunci: %s \n\n\n", kunci);
      fclose(publik);
      //write(sock, message, sizeof(message));
      write(sock, kunci, sizeof(kunci));
    }
    if(strcmp(message,"tes") == 0)
    {
      write(sock, message, sizeof(message));
    }
    else if(strcmp(message,"close") == 0 || strcmp(message,"open") == 0 || strcmp(message,"quit_server") == 0)
    {
      write(sock, message, sizeof(message));
    }
    else
    {
      goto sendrequestagain;
    }
  }
  /*if(strcmp(message,"key") == 0)
  {
      char kunci[200];
      FILE *publik;
      publik = fopen("public_client.txt", "r");
      fread(kunci, 200, 1, publik);
      fclose(publik);
      printf("\n\n\nkunci: %s \n\n\n", kunci);
 
      write(sock, message, sizeof(kunci));
  }*/
  else
  {
	/* message2 diisi dengan hasil encrypt perintah "knock"
	* - buat file ciphertext isinya encrypt(knock)
	* - baca file ciphertext tsb
	* - masukkan ke variabel message2
	* - kirim message2 ke port yang di-knock
	* */
	
	//-- encrypt client_message;
	strcpy( message2, "knock" );
	char shell_command[200], file_cipher[] = "msg2.enc";
	char file_knock[] = "knock.txt";
	strcpy( shell_command, "./rsa -f encrypt " );
	//strcat( shell_command, message2 );
	strcat( shell_command, file_knock );
	strcat( shell_command, " " );
	//strcat( shell_command, file_cipher );
	strcat( shell_command, file_cipher );
	printf( "Shell command: %s", shell_command );
	system( shell_command );
        //exit(1);
        
	
	//-- baca hasil encrypt
	FILE *filename;
	char buffer[200];
	filename = fopen(file_cipher, "rb");
	fread(&buffer, 200, 1, filename);
	fclose(filename);
	//strcpy( message2, strcpy( "enc", buffer) );
        printf("msg2: %s ", message2);
        //printf("msg: %s ", buffer);
	
	//-- kirim hasil encrypt
	write(sock, buffer, sizeof(buffer));
        //exit(1);
  }
  terima:

  while(1)
  {
    if( recv(sock , server_reply , 2000 , 0 ) < 0)
    {
      puts("recv failed");
      break;
    }			
    if(strcmp(server_reply, "") != 0)	
    {
      if(strcmp(message,"close") == 0 || strcmp(message,"open") == 0 || strcmp(message,"quit_server") == 0)
      {
        goto sendrequestagain;
      }
      else
      {  
        if(strcmp(server_reply, "end") == 0)
        {
          printf("\nServer reply : ");
          printf("%s\n", server_reply);
          printf("\nMenutup Socket...\n");
          port_baru = port_default;
          close(sock);
          sleep(3);
          goto koneklagi;
          break;
        }
        else
        {
          char temp[5];
          temp[0] = server_reply[0];
          temp[1] = server_reply[1];
          temp[2] = server_reply[2];
          temp[3] = server_reply[3];
          temp[4] = server_reply[4];
          printf("\nServer reply : ");
          //printf("%zu\n", sizeof(temp));
	for(int i = 0; i<5;i++)
	{
		printf("%c", temp[i]);
	}
	printf("\n");	
        size_text = strlen(server_reply);
        port_baru = atoi(server_reply);
        requestkembali:
	printf("port will connect in : %d\n", port_baru);
       	sleep(3);
	close(sock);
	goto koneklagi;
	}
      }
    }
    else
    {
      goto sendrequestagain;
    }
    close(sock);
    //fclose(publik);
    return 0;
  }
}
