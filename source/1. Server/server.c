#include <stdio.h>
#include <string.h>
#include <ncurses.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>	
#include <time.h>
#include <math.h>
#include "state.txt"

//-----------inisialisasi variabel server---------------
typedef unsigned int uint;
int counter;
char arrPort[10000][5];
int total = 0;
int counter = 0, counter2 = 0, size_text;
int flag_oc = 0;
void myrandom();
void get_rand_from_file();
void remove_string();

//-----------inisialisasi variabel RSA---------------
long int flag,temp[100],j,i,pr,k;
char m[100], en[100], pesan[100], pilih;
long int  prima1, prima2, a,status,cek;
int p, q, n, t, e[100],d[100];
int prime(long int);
long int cd(long int);
void ce();
void random_prima();
void encrypt();
void decrypt();

int main(int argc, char *argv[])
{
  int socket_desc, client_sock, client_sock2, c, c2, read_size, read_size2, sock2;
  struct sockaddr_in server, client, server2, client2;
  char client_message[2000] = {0};
  char client_message2[2000] = {0};
  int cont = 1;
  int port_baru, port_default;
  int last_socket = 0;
  char temp[5], close_msg[5];

char response_ok[2000] = "HTTP/1.1 200 Ok\nContent-type:text/html\n\n<html>\n<body>\n<h1>Server Hello</h1>\n</body>\n</html>\n";

  //char response_ok[2000] df= "HTTP/1.1 200 Ok\nContent-type:text/html\n\n<html>\n<body>\n<h1>Server Hello</h1>\n</body>\n</html>\n";
  size_t i;

  close_msg[0] = 'e';
  close_msg[1] = 'n';
  close_msg[2] = 'd';
	
  printf("\nMenutup Port 23...\n");
  sleep(2);
  system("iptables -A INPUT -p tcp --dport 23 -j REJECT");
  system("iptables -I INPUT -p tcp --dport 23 -j REJECT");
  printf("Port 23 Ditutup\n\n");
	
  //initialization
  int cnt = 0;
  port_default = 80;
  port_baru = port_default;
  koneklagi:

  //membuat socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if(setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
  {
    perror("setsockopt(SO_REUSEADDR) failed");
  }
  if(socket_desc == -1)
  {
    printf("tidak bisa membuat socket");
  }
  puts("Socket telah dibuat");
	 
  //prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons( port_baru );
		
  //bind
  rebind:
  if( bind(socket_desc, (struct sockaddr *)&server , sizeof(server)) < 0)
  {
  
    //print the error message
    perror("bind failed. Error");
    printf("\nServer will rebind in 3 seconds...\n");
    close(socket_desc);
    sleep(1);
    goto koneklagi;
  }
  puts("Bind sukses");
	
  //listen
  listen(socket_desc, 3);	
  tunggu:

  //accept and incoming connection
  puts("Menunggu koneksi masuk...");
  c = sizeof(struct sockaddr_in);
	
  //accept connection perform an incoming client
  client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
  if(client_sock < 0)
  {
    goto tunggu;
  }
  puts("Connection accepted");
	
  //receive a message from client
  while(cont == 1)
  {
    printf("\nMenunggu.....\n");
    read_size = read(client_sock, client_message, 2000);
    printf("msg %d: %s \n", cnt, client_message);
    remove_string(client_message);

    //cek karakter
    printf("%ld",strlen(client_message));
		
    //cek ASCII
    for(i=0;i<strlen(client_message);i++)
    {
      printf("%d  ",client_message[i]);
    }
    if(strcmp(client_message, "close") == 0)
    {
      if(flag_oc == 1)
      {
        printf("\n Closing Port 23....\n");
	sleep(2);
	system("iptables -A INPUT -p tcp --dport 23 -j REJECT");
	system("iptables -I INPUT -p tcp --dport 23 -j REJECT");
	printf("Port 23 Closed\n\n");
      }
      else
      {
	printf("\nYou have to knocking port first\n");
      }
    }
    if(strcmp(client_message, "open") == 0)
    {
      if(flag_oc == 1)
      {
	printf("\n Opening port 23...\n");
	sleep(2);
	system("iptables -A INPUT -p tcp --dport 23 -j ACCEPT");
	system("iptables -I INPUT -p tcp --dport 23 -j ACCEPT");
	printf("Port 23 opened\n\n");
      }
      else
      {
	printf("\nYou have to knocking port first\n");
      }
    }
    if(strcmp(client_message, "quit_server") == 0)
    {
      return 1;
      break;
    }

    if(strcmp(client_message, "tes") == 0)
    {
      printf("\nMelakukan Proses Random Port...\n");
      myrandom();
      get_rand_from_file();
      sleep(1);
      printf("\nRandom Port Siap...\n");
      cnt = 0;
      sleep(2);
      system("iptables -A INPUT -p tcp --dport 23 -j REJECT");
      system("iptables -I INPUT -p tcp --dport 23 -j REJECT");
      printf("Port 23 Closed\n\n");
      write(client_sock, arrPort[cnt], 5);

      //Penyimpanan port sementara
      char temp[5];
      temp[0] = arrPort[cnt][0];
      temp[1] = arrPort[cnt][1];
      temp[2] = arrPort[cnt][2];
      temp[3] = arrPort[cnt][3];
      temp[4] = arrPort[cnt][4];
      port_baru = atoi(temp);
      cnt++;
      close(socket_desc);
      printf("\nServer Listen pada Port : %d\n", port_baru);
      printf("\n\n");
      goto koneklagi;
    }

    if(client_message[0] == 'G' && client_message[1] == 'E' && client_message[2] == 'T')
    {
      write(client_sock, response_ok, 2000);
      close(client_sock);
      close(socket_desc);
      printf("%d\n", port_baru);
      goto koneklagi;
    }
    if(strcmp(client_message, "knock") == 0)
    {
      if(cnt == 10)
      //if(cnt == total)
      {
        printf("\nPort telah habis...\n");
        flag_oc = 1;
        write(client_sock, close_msg, 5);
        printf("\nMembuka Port 23...\n");
        sleep(2);
        system("iptables -A INPUT -p tcp --dport 23 -j ACCEPT");
        system("iptables -I INPUT -p tcp --dport 23 -j ACCEPT");
        printf("Port 23 Opened\n\n");
        port_baru = port_default;
        close(client_sock);
        close(socket_desc);
        goto koneklagi;
      }
      printf("\nKnock sukses pada port : %d\n",port_baru);
      write(client_sock, arrPort[cnt], 5);
      char temp[5];
      temp[0] = arrPort[cnt][0];
      temp[1] = arrPort[cnt][1];
      temp[2] = arrPort[cnt][2];
      temp[3] = arrPort[cnt][3];
      temp[4] = arrPort[cnt][4];
      port_baru=atoi(temp);
      cnt++;
      close(socket_desc);
      printf("\nServer Listen pada Port : %d\n",port_baru);
      printf("\n");



      goto koneklagi;
    }
    if(strcmp(client_message, "quit") == 0)
    {
      close(client_sock);
      close(socket_desc);
      goto koneklagi;
    }
    if(strcmp(client_message, "") == 0)
    {
      close(client_sock);
      close(socket_desc);
      goto koneklagi;
    }
    else
    {
      char t_msg[5];
      write(client_sock, t_msg, 5);
    }
  }
 goto koneklagi;
}

		
void get_rand_from_file()
{
  int counter;
  char ports[65000];
  char* xxx;
  char* filename ="./acak.txt";
	
  FILE* file = fopen(filename, "r");
  char line[65000];
  while(fgets(line, sizeof(line), file))
  {
    strcpy(ports, line);
  }
  fclose(file);
  size_text = strlen(ports);
  counter = 0;
  total = 0; 
  while(1)
  {
    if(ports[counter] == ',')
    {
      total++;
    }
    if(counter == size_text)
    {
      break;
    }
    counter++;
  }

  //sebagai pemecah koma
  counter=0; 
  
  //sebagai pemecah port
  counter2=0;

  //untuk 5 angka port
  int counter3 = 0;
  while(counter < size_text)
  {
    if(ports[counter] == ',')
    {
      counter2++;
      counter3=0;
      arrPort[counter2][0] = ' ';
      arrPort[counter2][1] = ' ';
      arrPort[counter2][2] = ' ';
      arrPort[counter2][3] = ' ';
      arrPort[counter2][4] = ' ';
    }
    else
    {
      arrPort[counter2][counter3] = ports[counter];
      counter3++;
    }
    counter++;
  }
}
	
typedef struct 
{ 
  uint64_t state;  
  uint64_t increment; 
  uint64_t multiplier;
}
 
pcg32_random_t;
//static uint64_t state = 0xcafef00dd15ea5e5;
static uint64_t const multiplier = 6364136223846794005u;
static uint64_t const increment = 1442695040888963407u;	

uint32_t pcg32_random_r(pcg32_random_t* rng)
{
  uint64_t oldstate = rng->state;
  rng->state = oldstate * multiplier + (rng->increment|1);
  uint32_t xorshifted = ((oldstate >> 18) ^ oldstate) >> 27;
  uint32_t rot = oldstate >> 59;
  return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

void pcg32_srandom_r(pcg32_random_t* rng, uint64_t initstate, uint64_t initseq)
{
  rng->state = 0u;
  rng->increment = (initseq << 1u) | 1u;
  pcg32_random_r(rng);
  rng->state += initstate;
  pcg32_random_r(rng);
}

void myrandom()
{
  FILE *filea;
  FILE *fileb;
  int random_port, random, flag=0,j;
  int max_rand_bil = 0;
  int counter = 0;
  int cntr = 0;
  int temp_random[10000];
  char buffer[99999];
  char portrandom[99999];
  size_t i;
  //static uint64_t state = 0x0000b711;
	
  filea = fopen("acak.txt", "w");
  fileb = fopen("state.txt", "w");
  pcg32_random_t pcg;
  //pcg32_srandom_r(&pcg, multiplier, increment);
  pcg32_srandom_r(&pcg, state, multiplier);
	
  max_rand_bil = (pcg32_random_r(&pcg) % 100 + 1);
  //max_rand_bil = (pcg32_random_r(&pcg) % 10000) + 1259;
  //max_rand_bil = pcg32_random_r(&pcg) % 65000;
  while(1)
  {
    random_port = pcg32_random_r(&pcg) % 65000; //hasil port < 65.000
    flag = 0;
    for(i = 0; i < max_rand_bil; i++)
    {
      if(random_port == temp_random[i])
      {
	flag = 1;
      }
      if (random_port == 8080 || random_port == 23 || random_port == 80 || random_port == 23)
      {
        flag = 1;
      }
    }
    if(flag == 1)
    {
      continue;
    }
    else
    {
      random = random_port;
      //printf("\n\nrandom_port: %d",random);
      sprintf(portrandom,"%d", random);
      random_prima();
      //enkripsi port
      char *pesan = portrandom;
      //printf("\nport yang akan dienkripsi: %s",pesan);
      for (i=0;pesan[i]!='\0';i++)
      {
        m[i]=pesan[i]; // menampung nilai yang sudah ada dlam pesan
      }
      encrypt(pesan, m, e);
      //decrypt();
      //getch();
      //sprintf(buffer,"%d, ", random_port); //untuk decimal

      for(i=0;en[i]!=-1;i++)
      {
        fprintf(filea,"%c",en[i]);
      }
      fprintf(filea,", ");

      //fwrite(buffer, strlen(buffer), 1, filea);
      temp_random[j] = random_port;
      temp_random[j] = random_port;
      cntr++;
    }
    if(cntr == max_rand_bil)
    {
      break;
    }
  }
  //printf("random yg dimasukkan : %0x%x", temp_random[i]);
  fprintf(fileb, "static uint64_t state = 0x%08x;", temp_random[0]);
  fclose(filea);
  fclose(fileb);
}

//fungsi hapus enter
void remove_string(char *str)
{
  if(str == NULL)
  return;
  int length = strlen(str);
  if(str[length-1] == '\n')
  str[length-1] = '\0';
}

int prime(long int pr)
{
  int i;
  j=sqrt(pr);
  for (i=2;i<=j;i++)
  {
    if(pr%i==0)
    return 0;
  }
  return 1;
}

void ce()
{
  int k;
  k=0;
  for (i=2;i<t;i++)
  {
    if(t%i==0)
    continue;
    flag=prime(i);
    if(flag==1&&i!=p&&i!=q)
    {
      e[k]=i; //nilai enkripsi
      flag=cd(e[k]);
      if(flag>0)
      {
        d[k]=flag; //nilai dekripsi
        k++;
      }
      if(k==99)
      break;
    }
  }
}

long int cd(long int x)
{
  long int k=1;
  while(1)
  {
    k=k+t;
    if(k%x==0)
    return(k/x);
  }
}

void encrypt(char pesan[], char m[], int e[])
{
  int pt,ct,key=e[0],k,len; //pt yaitu plainteks
  i=0;
  len=strlen(pesan); //menghitung panjang karakter dalam suatu string
  while(i!=len) // program akan selesai jika nilai len sudah berakhir
  {
    pt=m[i]; //menampung nilai message ke variabel pt
    pt=pt-96; //message -96
    k=1;
    for (j=0;j<key;j++)
    {
        k=k*pt;
        k=k%n;
    }
    temp[i]=k; //menampung nilai k
    ct=k+96;
    en[i]=ct;
    i++;
  }
  en[i]=-1;
  /*printf("\n\nNILAI ENKRIPSI : \n");
  for (i=0;en[i]!=-1;i++)
  {
    printf("%c",en[i]);
  }
  printf("\n");*/
}

void decrypt()
{
  long int pt,ct,key=d[0],k;
  i=0;
  while(en[i]!=-1)
  {
    ct=temp[i];
    k=1;
    for (j=0;j<key;j++)
    {
        k=k*ct;
        k=k%n;
    }
    pt=k+96;
    m[i]=pt;
    i++;
  }
  m[i]=-1;
  /*printf("\nNILAI DEKRIPSI\n");
  for (i=0;m[i]!=-1;i++)
  {
    printf("%c",m[i]);
  }
  printf("\n");*/
}

void random_prima(int p, int q)
{
  int i,number[1000]={0},temp,j,status=0, prime[1000]={0},r;
  for(i=2;i<=1000;i++)
  {
    for(j=i+1;j<=1000;j++)
    {
      if(j % i == 0 && number[j] == 0)
      {
        number[j] = 1;
      }
    }
  }
  temp = 0;
  for(i=2;i<=1000;i++)
  {
    if(number[i] == 0)
    {
      prime[temp++] = i;
    }
  }
  srand(time(NULL));
  do
  {
    r = rand()%1000;
  }
  while(prime[r] == 0);
  p = prime[r];
  do
  {
    r = rand()%1000;
  }
  while(prime[r] == 0 || prime[r] == p);
  {   
    q = prime[r];
  }
  //printf("\nBilangan Prima 1 : %d",p); 
  //printf("\nBilangan Prima 2 : %d",q);

  n=p*q; // perkalian pembangkitan kunci
  printf("\nn = %d",n);
  t=(p-1)*(q-1); //nilai untuk mencari relatif prima kunci

  ce();
  printf("\ne: %d",e[0]); 
}
