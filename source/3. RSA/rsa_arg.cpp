#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

long int flag,temp[100],j,i,pr,k;
char m[100], en[100], pesan[100];
int p = 53;
int q = 929;
int n = 49237;
int e = 3;
int d = 32171;
void encrypt();
void decrypt();

int main(int argc, char** argv)
{
  printf("--------Program Perhitungan RSA-------\n\n");

  //---------------message yg akan dienkripsi--------------------

  if (argc > 1) 
  {
    char *pesan = argv[1];
  } 
  else 
  {
    printf("Masukkan pesan: ");
    scanf("%s",pesan);
  }
  for (i=0;pesan[i]!='\0';i++)
  {
    m[i]=pesan[i]; // menampung nilai yang sudah ada dlam pesan
  }
  encrypt();
  decrypt();
  getch();
}

//------------------enkripsi pesan----------------------
void encrypt()
{
  int pt,ct,key=e,k,len;
  i=0;
  len=strlen(pesan); //menghitung panjang karakter dalam suatu string
  while(i!=len) // program akan selesai jika nilai len sudah berakhir
  {
    pt=m[i]; //menampung nilai message ke variabel pt
    pt=pt-96; 
    k=1;
    for (j=0;j<key;j++)
    {
        k=k*pt;
        k=k%n;
    }
    temp[i]=k; //menampung nilai k
    ct=k+96;
    en[i]=ct;
    i++;
  }
  en[i]=-1;
  FILE *filea;
  filea=fopen("enkripsi.txt","w");
  /*for (i=0;en[i]!=-1;i++)
  {
    printf("%c",en[i]);
    //fprintf(filea,"%c",en[i]);
  }*/
  fprintf(filea,"%s",en);
  fclose(filea);
}

//------------------dekripsi pesan----------------------
void decrypt()
{
  long int pt,ct,key=d,k;
  i=0;
  while(en[i]!=-1)
  {
    ct=temp[i];
    k=1;
    for (j=0;j<key;j++)
    {
        k=k*ct;
        k=k%n;
    }
    pt=k+96;
    m[i]=pt;
    i++;
  }
  m[i]=-1;
  printf("\nNilai Dekripsi: ");
  for (i=0;m[i]!=-1;i++)
  {
    printf("%c",m[i]);
  }
  FILE *fileb;
  fileb=fopen("dekripsi.txt","w");
  fprintf(fileb,"%s",m);
  fclose(fileb);
  printf("\n");
}
