#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

long int flag,e[100],d[100],temp[100],j,i,pr,k;
char m[100], en[100], pesan[100], pilih;
long int  prima1, prima2, a,status,cek;
int p, q, n, t;

int prime(long int);
long int cd(long int);
void ce();
void encrypt();
void decrypt();

int main(int argc, char* argv[])
{
	FILE *fp;
	printf("--------Program Perhitungan RSA-------\n\n");

	//menentukan bilangan prima
	srand(time(NULL));

do
{
  printf("Bilangan Prima Pertama: ");
  p=rand()%100;//batas random sampai 100
  prima1 = p%2;
  printf("%d",p);
  for(a=1; a<=p; a++)
  {
    status =0;
    for(pr = 1; pr <= a; pr++)
    {
      if(a % pr == 0)
      {
        status++;
      }
    }
  }
  if(status != 2)
  {
    printf("\t(Salah)");
    printf("\nApakah Ingin Random Lagi?(Y|N): ");
    scanf("%c",&pilih);
    printf("\n");
    if(pilih=='N'||pilih=='n')
    {
      exit(0);
    }
  }
  else
  {
    printf("\t(Benar)");
  }
}
while(status!=2);
printf("\n\n");

do
{
  status =0;
  printf("Bilangan Prima Kedua: ");
  q=rand()%100;//batas random sampai 100
  prima2 = q%2;
  printf("%d",q);
  for(a=1; a<=q; a++)
  {
    status =0;
    for(pr = 1; pr <= a; pr++)
    {
      if(a % pr == 0)
      {
        status++;
      }
    }
  }
  if(status!=2)
  {
    printf("\t(Salah)");
    printf("\nApakah Ingin Random Lagi?(Y|N): ");
    scanf("%c",&pilih);
    printf("\n");
    if(pilih=='N'||pilih=='n')
    {
      exit(0);
    }
  }
  else
  {
    printf("\t(Benar)");
  }
}
while(status!=2);
printf("\n\n");

if (p==q)
{
  exit(0);
}
printf("Bilangan Prima 1 : %d",p);
printf("\nBilangan Prima 2 : %d",q);
printf("\n----------------------------------------\n");

//---------------message yg akan dienkripsi--------------------

if (argc > 1) {
	pesan = argv[1];
} else {
	printf("ENTER MESSAGE : ");
	scanf("%s",pesan);
}

for (i=0;pesan[i]!='\0';i++)
{
  m[i]=pesan[i]; // menampung nilai yang sudah ada dlam pesan
}
n=p*q; // perkalian pembangkitan kunci
printf("\n n = %d \n",n);
t=(p-1)*(q-1); //nilai untuk mencari relatif prima kunci
printf("totient (n)  = %d \n",t);
ce(); //cek nilai enkripsi
printf("\nANGKA TERSEDIA DARI E DAN D");
for (i=0;i<j-1;i++)
{
  printf("\nNILAI E: %ld\t  NILAI D: %ld",e[i],d[i]);
}
encrypt();
fp = fopen ("sisi.txt", "w");
fprintf (fp,"hasil \n %s",en);
decrypt();
getch();
}

int prime(long int pr)
{
	int i;
	j=sqrt(pr);
	
	for (i=2;i<=j;i++)
	{
		if(pr%i==0)
		return 0;
	}
	
	return 1;
}


void ce()
{
	int k;
	k=0;
	
	for (i=2;i<t;i++)
	{
		if(t%i==0)
			continue;
		
		flag=prime(i);
		
		if(flag==1&&i!=p&&i!=q)
		{
			e[k]=i; //nilai enkripsi
			flag=cd(e[k]);
			
			if(flag>0)
			{
				d[k]=flag; //nilai dekripsi
				k++;
			}
			if(k==99)
				break;
		}
	}
}

long int cd(long int x)
{
	long int k=1;
	
	while(1)
	{
		k=k+t;
		if(k%x==0)
		return(k/x);
	}
}

//------------------enkripsi pesan----------------------
void encrypt()
{
	int pt,ct,key=e[0],k,len; //pt yaitu plainteks
	i=0;
	len=strlen(pesan); //menghitung panjang karakter dalam suatu string
	
	while(i!=len) // program akan selesai jika nilai len sudah berakhir
	{
		pt=m[i]; //menampung nilai message ke variabel pt
		pt=pt-96; //message -96
		k=1;
		
		for (j=0;j<key;j++)
		{
			k=k*pt;
			k=k%n;
		}
		
		temp[i]=k; //menampung nilai k
		ct=k+96;
		en[i]=ct;
		i++;
	}
	
	en[i]=-1;
	printf("\n----------------------------------------\n"); 
	printf("\nNILAI ENKRIPSI: ");
	printf("\n");
	
	for (i=0;en[i]!=-1;i++)
	{
		printf("%c",en[i]);
	}
	
	printf("\n");
}

//------------------dekripsi pesan----------------------
void decrypt()
{
	long int pt,ct,key=d[0],k;
	i=0;
	while(en[i]!=-1)
	{
		ct=temp[i];
		k=1;
		for (j=0;j<key;j++)
		{
			k=k*ct;
			k=k%n;
		}
		
		pt=k+96;
		m[i]=pt;
		i++;
	}
	
	m[i]=-1;
	printf("\nNILAI DEKRIPSI\n");
	
	for (i=0;m[i]!=-1;i++)
	{
		printf("%c",m[i]);
	}
	
	printf("\n");
}

//--EoF
