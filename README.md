*Project Tugas Akhir*

Port Knocking, dengan bahasa C.
Running on Linux 16.04.

Safira dan Ega
--------------

Safira:
PENGAMANAN PERANGKAT IOT DENGAN ENCRYPTED PORT KNOCKING
1.	a. Bagaimana meningkatkan keamanan port knocking dengan enkripsi?
    b. Bagaimana rancangan port knocking dengan enkripsi?
2.	a. Mengetahui peningkatan keamanan port knocking.   
    
Ega:
KEAMANAN JARINGAN PADA WEB MENGGUNAKAN ALGORITMA RSA DAN METODE PORT KNOCKING
1.  a. Bagaimana merancang sistem keamanan jaringan menggunakan metode port knocing.
    b. Bagaimana mengamankan informasi port yang sudah diketuk dengan menggunakan kriptografi RSA.
2.	a. Mengetahui pengaruh port knocking dalam peningkatan keamanan jaringan.
    b. Mengetahui pengaruh RSA dalam mengamankan port yang diketuk.
